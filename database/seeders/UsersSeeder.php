<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;


class UsersSeeder extends Seeder
{

    public function run()
    {
            $User = new User;
            $User->name = 'Elisa';
            $User->lastName = 'Lopez';
            $User->email = 'elope@gmail.com';
            $User->status = 1;
            $User->save();

            $User = new User;
            $User->name = 'Miroslavo';
            $User->lastName = 'Mirazo';
            $User->email = 'mirosl@gmail.com';
            $User->status = 1;
            $User->save();

            $User = new User;
            $User->name = 'Briseido';
            $User->lastName = 'Cazarez';
            $User->email = 'Bris@gmail.com';
            $User->status = 1;
            $User->save();
    }

}

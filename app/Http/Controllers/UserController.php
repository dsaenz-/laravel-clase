<?php

namespace App\Http\Controllers;

use App\Jobs\LoteUsuarios;
use App\Mail\ContactoMailable;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{

    public function getUsers(){

        try {
            return response()->json([
                'success' => true,
                'message' => "Registros obtenidos exitosamente",
                'data' => '1',
                'color' => 'success',
                'users' => User::all()
            ]);
        }catch (\Exception $e){
            return response()->json([
                'success'       =>false,
                'message'       =>"Ocurrió un error: ". $e->getMessage(),
                'data'          =>$e->getCode(),
                'color'         =>'danger'
            ]);
        }

    }

    public function index(){

       // $users = ['Luis' , 'Elisa', 'Juana', 'Miguel'];

        $users = User::all();

        $title = "Listado de empleados";

        return view('Users.index', compact('users', 'title') );
    }

    public function show($id){

        $user= User::findOrFail($id);

        return view('Users/show', compact('user'));
    }

    public function create(){
        return view('Users/create');
    }

    public function edit($id){

        $user = User::find($id);
        return view('Users/edit')->with('user', $user);
    }

    public function updateUser(Request $request){

        $user = User::find($request->id);
        $user->name         =   $request->nombre_v;

        if(!User::where('email', '=', $user->email )->exists()){
            $user->email        =   $request->correo_v;
        }

        $user->lastName     =   $request->apellido_v;
        $user->save();

        return redirect()->route('ver', $user->id);
    }


    public function store(Request $request){

        $data = request()->validate([
            'nombre_v'      => 'required|min:5',
            'correo_v'      => 'required',
            'apellido_v'    => 'required',
        ], [
            'nombre_v.required'     => 'El campo nombre es obligatorio',
            'nombre_v.min'     => 'no se cumple con el mínimo',
            'correo_v.required'     => 'El campo correo es obligatorio',
            'correo_v.email'        => 'Por favor ingresa una dirección valida',
            'correo_v.unique'       => 'Ya existe un usuario con ese email',
            'apellido_v.required'   => 'El campo apellido es obligatorio'
        ]);

        $user               =   new User();
        $user->name         =   $request->nombre_v;
        $user->email        =   $request->correo_v;
        $user->lastName     =   $request->apellido_v;
        $user->save();

      //  $correo = new ContactoMailable();
      //  Mail::to($request->correo_)->send($correo);

        //crear usuario mediante job, se necesita comando para escuchar jobs aparte
      //  LoteUsuarios::dispatch($request->nombre_v,$request->correo_v, $request->apellido_v );
        //return redirect()->route('ver', $user->id);



        return redirect()->route('todos');

    }

    public function destroy($id){
        User::where('id', $id)->delete();
        return redirect()->route('todos');
    }

    public function saludo($name, $nickname=null ){
        if($nickname != null){

             return view('saludo');
        }else{
            return "hola  $name  no tienes apodo ";
        }
    }
}

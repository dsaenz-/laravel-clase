<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LoteUsuarios implements ShouldQueue
{
    protected $nombre;
    protected $apellido;
    protected $correo;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name, $email, $lastName)
    {
        $this->nombre = $name;
        $this->apellido = $email;
        $this->correo = $lastName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
/*
        for ($i =0; $i<100000; $i++){
            $user               =   new User();
            $user->name         =   "imported - {$i}";
            $user->email        =   "imported{$i}@gmail.com";
            $user->lastName     =   "imported - {$i}";
            $user->save();
        }
*/
        $user               =   new User();
        $user->name         =   $this->nombre;
        $user->email        =   $this->correo;
        $user->lastName     =   $this->apellido;
        $user->save();


    }
}

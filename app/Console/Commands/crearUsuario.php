<?php

namespace App\Console\Commands;

use App\Jobs\LoteUsuarios;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class crearUsuario extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cmd:crearUsuario';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando sirve para probar que realmente se guardan usuarios';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /*
        $user               =   new User();
        $user->name         =   "Juanito";
        $user->email        =   "Juan@gmail.com";
        $user->lastName     =   "Ridríguez";
        $user->save();
*/

        LoteUsuarios::dispatch();

       // Log::debug();
        return 0;
    }
}

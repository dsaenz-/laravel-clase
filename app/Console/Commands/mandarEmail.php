<?php

namespace App\Console\Commands;

use App\Mail\ContactoMailable;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class mandarEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendMail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando desde la consola, manda un correo';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $correo = new ContactoMailable();

        Mail::to('dennis.ozwaldo@gmail.com')->send($correo);

        return 0;
    }
}

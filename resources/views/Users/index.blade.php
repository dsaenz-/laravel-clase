@extends('adminlte::page')


@section('title', 'Tópicos 3')

@section('content_header')
    <h1>Alta usuarios</h1>
@stop


@section('content')

    <h1> {{$title}} </h1>

    <a href=" {{ route('crear') }} " type="button" class="btn btn-primary my-3"
    >Nuevo</a>


    @empty($users)
        No hay registros
    @endempty

    <ul>
        @foreach ($users as $user)
            <li>

                <a href=" {{ route('ver', $user->id ) }} ">
                    {{$user->name}}
                </a>
                -
                <a class="link-danger" href=" {{ route('borrarUsuario', $user->id ) }} ">
                    Eliminar
                </a>

            </li>
        @endforeach

    </ul>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop


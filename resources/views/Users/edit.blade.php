@extends('layout')


@section('principal')

    <h1>Actualizando un usuario en el sistema</h1>

    <form method="POST" action="{{ route('actualizarUsuario') }}">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <input type="hidden" value=" {{ $user->id}}"  name="id">
        <div class="mb-3">
            <label for="name" class="form-label">Nombre</label>
            <input value=" {{ $user->name  }} " type="text" name="nombre_v" class="form-control" id="name" placeholder="ej. Juan Perez">

            @if ($errors->has('nombre_v'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('nombre_v') }}
                </div>
            @endif

        </div>

        <div class="mb-3">
            <label for="lastName" class="form-label">Apellido</label>
            <input value=" {{ $user->lastName  }} "  type="text" name="apellido_v" class="form-control" id="lastName" placeholder="Escribe aquí tu apellido">
            @if ($errors->has('apellido_v'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('apellido_v') }}
                </div>
            @endif

        </div>

        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input value=" {{ $user->email  }} "  type="email" class="form-control" id="email"  name="correo_v">
            @if ($errors->has('correo_v'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('correo_v') }}
                </div>
            @endif
        </div>

        <button type="submit" class="btn btn-primary">Actualizar</button>
    </form>


@endsection
